/**
 * Capitalize character in the given string.
 *
 * @param  {String} str
 * @return {String}
 */
module.exports = {
  capitalize: function(str) {
    return str.charAt(0).toUpperCase() + str.substr(1).toLowerCase();
  }
};