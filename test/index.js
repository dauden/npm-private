var should = require('chai').should(),
    util = require('../index'),
    capitalize = util.capitalize;

describe('#capitalize', function() {
  it('change "hello" into "Hello"', function() {
    capitalize('hello').should.equal('Hello');
  });
  
  it('change "hEllo" into "Hello"', function() {
    capitalize('hEllo').should.equal('Hello');
  });
  
  it('change "HELLO" into "Hello"', function() {
    capitalize('HELLO').should.equal('Hello');
  });

});