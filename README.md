A small library providing utility methods to capitalize String

## Installation

  npm install DauDen-capitalize --save

## Usage

  	var utility = require('DauDen-capitalize');

  	var str = 'hello world';
    var capitalize = utility.capitalize(str);

  	console.log(capitalize string' + capitalize);

## Tests

  npm test

## Contributing

In lieu of a formal styleguide, take care to maintain the existing coding style.
Add unit tests for any new or changed functionality. Lint and test your code.

## Release History

* 0.1.0 Initial release